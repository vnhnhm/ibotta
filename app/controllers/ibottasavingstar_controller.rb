class IbottasavingstarController < ApplicationController
    def delete
        Ibottasavingstar.delete_all
    end
    
    def save
        @ibotta = Ibotta.all
        @savingstar = Savingstar.all
        
        @ibotta.each do |x|
           @savingstar.each do |y|
               
              x_place_holder = x.title.split(" ")
              x_place_holder.delete("or")
              y_place_holder = y.title.split(" ")
              y_place_holder.delete("or")
              
              z = x_place_holder & y_place_holder
              
              if z.length >= 2 && !Ibottasavingstar.find_by(title:  x.title + " + " + y.title)
                    
                    l = 0
                    m = 0            

                    a = x.title.downcase.split(" ")
                    b = y.title.downcase.split(" ")

                    while l < a.length
                        while m < b.length
                            if a[l] == b[m]
                                a[l] = a[l].upcase
                                b[m] = b[m].upcase
                                
                            end
                            m +=1
                        end
                        m = 0
                        l +=1
                    end
                    a = a.join(" ")
                    b = b.join(" ")
                 Ibottasavingstar.create(title1: a, title2: b, link1: x.link, link2: y.link, value1: x.values, value2: y.value) 
              end
              
           end
       end
       @products = Ibottasavingstar.all
    end
    
    def show
        @products = Ibottasavingstar.all
    end
    
end
