class SavingstarController < ApplicationController
    
    def delete
        Savingstar.delete_all
        @products = Savingstar.all
    end

    # 11 is Index for descrip for early ones..length of 27 divs
    # 12 is Index for descrip for second half..length of 19 divs
    
    def save
        require 'watir'
        require 'phantomjs'
        
        @browser = Watir::Browser.new:phantomjs
        @browser.goto "https://savingstar.com/coupons"
    
        @products = @browser.divs(class: "coupon")
    
        @title_arr = []    
        @value_arr = []
        @id_arr = []
        
        
        @products.each do |x|
            
            link_placeholder = "https://savingstar.com/coupons" + "#" + x.id 
            
            if x.divs.length > 20 
                title_placholder = x.divs[11].text
                title_placholder = title_placholder.split(" ")
                title_placholder.delete("Save")
                title_placholder.delete("when")
                title_placholder.delete("you")
                title_placholder.delete("buy")
                title_placholder.delete("on")
                title_placholder.delete("on")
                title_placholder.delete("spend")
                title_placholder.delete("spend")
                value_placeholder = title_placholder[0]
                title_placholder.shift
                title_placholder.shift
                title_placholder.shift
                title_placholder[0] = title_placholder[0].capitalize
            else
                title_placholder = x.divs[12].text
                title_placholder = title_placholder.split(" ")
                title_placholder.delete("Save")
                title_placholder.delete("when")
                title_placholder.delete("you")
                title_placholder.delete("buy")
                title_placholder.delete("on")
                title_placholder.delete("on")
                title_placholder.delete("spend")
                title_placholder.delete("spend")
                value_placeholder = title_placholder[0]
                title_placholder.shift
                title_placholder[0] = title_placholder[0].capitalize
            end
            
            title_placholder = title_placholder.join(" ")
            
            Savingstar.create(title: title_placholder, value: value_placeholder, store: "Any", link: link_placeholder)
            
        end
        
        
         
    end

    def show
        @products = Savingstar.all
    end

end