class Checkout51Controller < ApplicationController
    
    def delete
        Checkout51.delete_all
        @products = Checkout51.all
    end
    
    def save
        require 'watir'
        require 'phantomjs'
        
        @browser = Watir::Browser.new:phantomjs
        @browser.goto "https://www.checkout51.com/offers"
        
        @browser.div(:class => "drop-control").when_present.click
        @browser.link(:text =>"United States").when_present.click
        
        @products = @browser.lis(class: "offer")
        
        @products.each do |x|
            Checkout51.create(title: x.divs[13].text, value: x.divs[14].text, link: 'https://www.checkout51.com/offers')
        end
        @products = Checkout51.all
    end
    
    def show
        @products = Checkout51.all
    end
    
    def practice
        
    end
    
end


# offer-details-html-29947

# @products = @browser.lis(class: "offer")

# Index: 11 Nutri-Grain* bars Any variety. $0.25 Cash Back

# Index: 12

# Index: 13 Nutri-Grain* bars Any variety.

# Index: 14 $0.25 Cash Back