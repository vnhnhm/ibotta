class IbottaController < ApplicationController
    def save
        require 'watir'
        require 'phantomjs'
        
        @browser = Watir::Browser.new:phantomjs
        @browser.goto "https://ibotta.com/rebates"
        @button = @browser.button(class: "see-more-label")
        
        Ibotta.delete_all
        x = 24
        y = 0
        while y < x 
          @button.click
          y += 1
        end
        
        @products = @browser.divs(class: "offer-card")

        @products.each do |a|
            if a.divs[2].text.split("").include?('%')
            
            else
                value_placeholder = a.divs[3].text.split(" ")
                value_placeholder.delete("cash")
                value_placeholder.delete("back")
                value_placeholder = value_placeholder.join(" ").split("")
                value_placeholder.delete("$")
                value_placeholder = value_placeholder.join("")
                
                Ibotta.create(
                  title: a.imgs[0].alt, 
                  values: value_placeholder, 
                  store: a.divs[5].text, 
                  link: a.links[0].href
                )
            end
        end
        @products = Ibotta.all
    end

    def show
      @products = Ibotta.all
    end
    
    # def destroy
      # Ibotta.delete_all
      # @products = Ibotta.all
    # end
    
    
    def practice
        
    end
    
    def destroy
        Ibotta.find(params[:id]).destroy
        redirect_to ibotta_path
    end
    
end
