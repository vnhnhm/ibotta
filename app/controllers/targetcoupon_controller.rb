class TargetcouponController < ApplicationController

    def save
        require 'watir'
        require 'phantomjs'
        
        @browser = Watir::Browser.new:phantomjs
        @browser.goto "http://coupons.target.com/"
        @button = @browser.button(class: "more-button")
    
        x = 18
        y = 0
        while y < x 
        @button.click
        y+=1
        end
        
        @products = @browser.divs(class: "pod")
        
        @products.each do |a| 
            if Targetcoupon.find_by title: a.divs[3].text
            
            else
                title_placeholder = a.divs[3].text.split(" ")
                title_placeholder.delete("when")
                title_placeholder.delete("you")
                title_placeholder.delete("buy")
                title_placeholder.delete("on")
                title_placeholder.delete("one")
                title_placeholder.delete("any")
                title_placeholder = title_placeholder.join(" ")
                
                value_placeholder = a.divs[1].text.split(" ")
                value_placeholder.delete("SAVE")
                value_placeholder.delete("ON")
                value_placeholder.delete("ONE")
                value_placeholder.delete("TWO")
                value_placeholder.delete("OFF")
                value_placeholder.delete("THREE")
                value_placeholder.delete("FOUR")
                value_placeholder.delete("FIVE")
                value_placeholder = value_placeholder.join("").split("")
                value_placeholder.delete("$")
                if value_placeholder.include?("¢")
                    value_placeholder.delete("¢")
                    value_placeholder.unshift('.')
                end
                value_placeholder = value_placeholder.join("")
                Targetcoupon.create(store: "Manufacturer", link: "http://coupons.target.com/", title: title_placeholder, value: value_placeholder)
            end
        end
        @productsLength = Targetcoupon.all
    end
    
    
    
    def show
        @products = Targetcoupon.all
    end
    
    
    
    def delete
        Targetcoupon.delete_all
        @products = Targetcoupon.all
    end
    
    
    
    def practice
        require 'watir'
        require 'phantomjs'
        
        @browser = Watir::Browser.new:phantomjs
        @browser.goto "http://coupons.target.com/"
        @button = @browser.button(class: "more-button")
    
        x = 18
        y = 0
        while y < x 
        @button.click
        y+=1
        end
        
        @products = @browser.divs(class: "pod")
        
        @products.each do |a| 
            Targetcoupon.create(title: a.divs[3].text, value: a.divs[1].text)
        end
        
        @showproducts = Targetcoupon.all
        
    end
end