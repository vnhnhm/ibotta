class Ibottacheckout51Controller < ApplicationController
    def show
        @products = Ibottacheckout51.all
    end

    def delete
        Ibottacheckout51.delete_all
        @products = Ibottacheckout51.all
    end


    def save
        @checkout51 = Checkout51.all
        @ibotta = Ibotta.all
        
        @checkout51.each do |x|
           @ibotta.each do |y|
               
              z = x.title.split(" ") & y.title.split(" ")
              
              if z.length >= 2
                  
                    l = 0             
                    m = 0             

                    a = x.title.downcase.split(" ")
                    b = y.title.downcase.split(" ")

                    while l < a.length
                        while m < b.length
                            if a[l] == b[m]
                                a[l] = a[l].upcase
                                b[m] = b[m].upcase
                            end
                            m +=1
                        end
                        m = 0
                        l +=1
                    end
                    
                    a = a.join(" ")
                    b = b.join(" ")
                 Ibottacheckout51.create(title1: a, title2: b, value1: x.value, value2: y.values, link1: x.link, link2: y.link) 
              end
              
           end
        end

        @products = Ibottacheckout51.all
    end
    
end
