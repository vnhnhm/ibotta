class TargetibottumController < ApplicationController
    def show
        @products = Targetibottum.all
    end

    def delete
        Targetibottum.delete_all
        @products = Targetibottum.all
    end

#83 in iBotta (seems to match up)
#192 in target (seems to match up)

    def save
        @target = Targetcoupon.all
        @ibotta = Ibotta.all
        
        @target.each do |x|
           @ibotta.each do |y|
               
              z = x.title.split(" ") & y.title.split(" ")
              
              if z.length >= 2
                  
                    l = 0             
                    m = 0             

                    a = x.title.downcase.split(" ")
                    b = y.title.downcase.split(" ")

                    while l < a.length
                        while m < b.length
                            if a[l] == b[m]
                                a[l] = a[l].upcase
                                b[m] = b[m].upcase
                            end
                            m +=1
                        end
                        m = 0
                        l +=1
                    end
                    
                    a = a.join(" ")
                    b = b.join(" ")
                 Targetibottum.create(title1: a, title2: b, value1: x.value, value2: y.values, link1: x.link, link2: y.link) 
              end
              
           end
        end
        # Targetibottum.create(title1: "test title 1", title2: "test title 2", value1: "test value 1", value2: "test value 2")
        @products = Targetibottum.all
    end
    
end
