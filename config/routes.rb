Rails.application.routes.draw do
  resources :ibotta
  
  get 'scraper/ibotta'
  get 'scraper/checkout51'
  get 'ibotta/save'
  get 'ibotta/show'
  get 'ibotta/delete'
  get 'targetcoupon/save'
  get 'targetcoupon/delete'
  get 'targetcoupon/show'
  get 'targetibottum/delete'
  get 'targetibottum/show'
  get 'targetibottum/save'
  get 'savingstar/delete'
  get 'savingstar/save'
  get 'savingstar/show'
  get 'ibottasavingstar/show'
  get 'ibottasavingstar/save'
  get 'ibottasavingstar/delete'
  get 'targetcoupon/practice'
  get 'targetibottasavingstar/show'
  get 'targetibottasavingstar/save'
  get 'targetibottasavingstar/delete'
  get 'checkout51/save'  
  get 'checkout51/show' 
  get 'checkout51/delete'
  get 'checkout51/practice'
  
  get 'ibotta/practice'
  
  get 'ibottacheckout51/save'  
  get 'ibottacheckout51/show' 
  get 'ibottacheckout51/delete'
  
  get 'ibottacheckout51/practice'  
  
  get 'newcheckout51/save'  
  get 'newcheckout51/show' 
  get 'newcheckout51/delete'
  
  get 'smiths/save'
  get 'smiths/show'
  get 'smiths/delete'
  get 'smiths/practice'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
