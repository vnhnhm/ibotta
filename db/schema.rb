# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170905021807) do

  create_table "articles", force: :cascade do |t|
    t.string   "title"
    t.text     "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "checkout51__forces", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "checkout51s", force: :cascade do |t|
    t.string   "title"
    t.string   "value"
    t.string   "store"
    t.string   "link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ibotta", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "values"
    t.string   "store"
    t.string   "link"
    t.string   "string"
  end

  create_table "ibottacheckout51s", force: :cascade do |t|
    t.string   "title1"
    t.string   "title2"
    t.string   "value1"
    t.string   "value2"
    t.string   "link1"
    t.string   "link2"
    t.string   "store1"
    t.string   "store2"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ibottasavingstars", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "title1"
    t.string   "title2"
    t.string   "link1"
    t.string   "string"
    t.string   "link2"
    t.string   "value1"
    t.string   "value2"
  end

  create_table "newcheckout51s", force: :cascade do |t|
    t.string   "title"
    t.string   "value"
    t.string   "store"
    t.string   "link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "savingstar", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "savingstars", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "title"
    t.string   "link"
    t.string   "string"
    t.string   "value"
    t.string   "store"
  end

  create_table "smiths", force: :cascade do |t|
    t.string   "title"
    t.string   "value"
    t.string   "link"
    t.string   "store"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "targetcoupons", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "value"
    t.string   "link"
    t.string   "string"
    t.string   "store"
  end

  create_table "targetibotta", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "title1"
    t.string   "title2"
    t.string   "string"
    t.string   "value1"
    t.string   "value2"
    t.string   "link1"
    t.string   "link2"
  end

  create_table "targetibottasavingstars", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "title1"
    t.string   "title2"
    t.string   "title3"
    t.string   "value1"
    t.string   "value2"
    t.string   "value3"
    t.string   "link1"
    t.string   "link2"
    t.string   "link3"
  end

  create_table "targetibottum", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
